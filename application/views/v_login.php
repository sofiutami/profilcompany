<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Log in</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/login/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
   
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/login/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/login/iCheck/square/blue.css">

  <div class="row">
  <div class="col-md-4">
      <body class="hold-transition login-page">
        <div class="login-box">
        <div class="login-box-body">
            <form class="form-signin" method="POST" align="center" action="<?php echo base_url()?>index.php/login">
            <img class="mb-4" src="https://getbootstrap.com/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72">
            <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
            <label for="inputUsername" class="sr-only">Username</label>
            <input type="text" name="username"  id="inputUsername  " class="form-control" placeholder="Username" required="" autofocus="">
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="text" password="password" name="password" id="inputPassword" class="form-control" placeholder="Password" required="">
                <div class="checkbox mb-3">
                      <label>
                        <input type="checkbox" value="remember-me"> Remember me
                      </label>
                </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
  
         </div>
        </div>
  </div>
</body>
</html>
<style>
  .font-weight-normal{
    font-family:  Verdana, sans-serif;
  }
</style>