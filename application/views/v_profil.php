<!DOCTYPE>
<html>
<head>
    <title>hasil</title>

            <link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>admin/assets/view.css">
            <script src="http://localhost/profilcompany/admin/jquery-3.3.1.js" type="text/javascript"></script>
</head>
<body>
  <div id="notifikasi" style="display:none"></div>
    <div id="wraper">
    
        <div id="header">
            <h2>Jagalab</h2>
        </div>
         
            <div id="content">
            <div class="btn-outline-*" style="margin-top: 2%;">
            <div id="paneltombol">
                <input type="button" class="tombolTambah" id="btnSimpan" value="Simpan" style="display:none">
                <input type="button" class="tombolTambah" id="tombolUpdate" name="tombolUpdate" value="Update" style="display:none">
            </div>
        </div>
            <div id="data"> 

                <table border="0" width="100%" cellspacing="0" >
                <tr>
                    <th class="no">NO</th>
                    <th>Judul</th>
                    <th>Isi</th> 
                </tr>
                <?php foreach ($profil as $baris):?>
                <div class="btn-outline-*" style="margin-top: 2%;">
                <tr>
                    <td><?php echo $baris['no_profil'] ?></td>
                    <td><?php echo $baris['judul_profil'] ?></td>
                    <td><?php echo $baris['isi_profil']?></td>
                    <td width="30"><input type="button" class="tombol" name="tombol" value="Edit" onclick="edit(<?php echo $baris['no_profil'] ?>)"></td>

                     <td width="30"><input type="button" class="tombolhapus" name="tombol" value="Hapus" onclick="hapus(<?php echo $baris['no_profil'] ?>)"></td>
                <?php endforeach ?>
                </tr>
            </div>
                </table>
            </div>
        </div>    
    </div>
</body>
</html>

<script type="text/javascript" >
        $("#tombolTambah").click(function(){
            $.ajax({
                url : "<?php base_url() ?>company/tambah",
                beforeSend: function(){
                                    $("#data").html("Loading...");
                                },
                success:    function(html){
                                $("#data").html(html);
                                $("#btnSimpan").show();
                                $("#tombolTambah").hide();
                            }                
            }); 
            });           
    $("#btnSimpan").click(function(){
   
   var no_profil      = $("#no_profil").val();
   var judul_profil   = $("#judul_profil").val();
   var isi_profil     = $("#isi_profil").val();         
   $.ajax({
          url :"<?php echo base_url() ?>index_admin.php/company/tambah",
          type:"post",
          beforeSend: function(){
                      $("#data").html("Loading...");
                                    },
          data: "no_profil="+no_profil+"&judul_profil="+judul_profil+"&isi_profil="+isi_profil,
          success: function(html){                   
                   $("#notifikasi").html('Data berhasil disimpan');
                   $("#data").load("company/index #data");
                   $("#notifikasi").fadeIn(2500);
                   $("#notifikasi").fadeOut(2500);  
                   $("#btnSimpan").hide();
                   $("#tombolTambah").show();
                                       
        }
    });
    });

    function edit(no_profil){
            $( document ).ready(function(){                                        
                $.ajax({
                    url : "<?php echo base_url() ?>index.php/company/edit/"+no_profil, 
                    type:"post",       
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                    },                
                    success:    function(html){
                                $("#tombolUpdate").show();                      
                                $("#tombolTambah").hide();                                                   
                                $("#data").html(html);                 
                                }                
            });                    
            });        
        }

        $("#tombolUpdate").click(function(){                    
                var no_profil      = $("#no_profil").val();
                var judul_profil   = $("#judul_profil").val();
                var isi_profil     = CKEDITOR.instances['isi_profil'].getData();               
                $.ajax({
                    url : "<?php echo base_url() ?>index.php/company/update",
                    type: "post",
                    beforeSend: function(){
                                            $("#data").html("Loading...");
                                        },
                    data    : "no_profil="+no_profil+"&judul_profil="+judul_profil+"&isi_profil="+isi_profil,
                    success:    function(html){
                                $("#tombolTambah").show();                                 
                                $("#tombolUpdate").hide();                                 
                                $("#data").load("index #data");
                                $("#notifikasi").html('Data berhasil diedit');                                  
                                $("#notifikasi").fadeIn(2500);
                                $("#notifikasi").fadeOut(2500);                
                            }                
        });            
        });

            function hapus(no_profil){
            if(confirm('Yakin Menghapus?')){
            $( document ).ready(function(){                                        
                $.ajax({                    
                    url : "<?php echo base_url() ?>index.php/company/delete/"+no_profil,        
                    beforeSend: function(){
                                        $("#data").html("Loading...");
                                    },                                                
                    success:    function(html){
                                $("#tombolUpdate").hide();                                 
                                $("#tombolTambah").show();                                 
                                $("#data").load("index #data");                                                                                                    
                                }                
            });                    
            });    
        }        
        }
</script>