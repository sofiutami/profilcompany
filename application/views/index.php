<!doctype html>
<html>
<head>
	<title>Web Company</title>	
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css?family=Rubik:300,400,500" rel="stylesheet">
	<link rel="dns-prefetch" href="//fonts.googleapis.com">
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,700&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" 	type="text/css" href="<?php echo base_url() ?>assets/css/style.css">
	<link rel="stylesheet"  href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/css/bootstrap.min.css">
	<link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/css/themify-icons.css">
	<link rel="stylesheet"  type="text/css" href="<?php echo base_url() ?>assets/css/owl.carousel.min.css">
	<link rel="stylesheet" 	type="text/css" href="<?php echo base_url() ?>assets/css/stylee.css">
	<link rel="stylesheet" 	type="text/css" href="<?php echo base_url() ?>assets/css/gaya.css">
	<link rel="stylesheet"	type="text/css" href="<?php echo base_url() ?>assets/css/bostrp.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

        
    

	<body data-spy="scroll" data-target="#navbar" data-offset="30">
<section>
    	<nav class="navbar navbar-expand-lg fixed-top navbar-light" style="background-color:#dfe4ea">
        <div class="container">

            <!-- Navbar: Brand -->
            <a class="navbar-brand d-lg-none" >Jagalab</a>

            <!-- Navbar: Toggler -->
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            
            <!-- Navbar: Collapse -->
            <div class="navbar-collapse collapse show" id="navbarSupportedContent" style="">

                <!-- Navbar navigation: Left -->
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="about-us.html">About Us</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="http://example.com" id="nav-item__menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           Employees
                        </a>
                        <div class="dropdown-menu" aria-labelledby="nav-item__menu">
                            <a class="dropdown-item" href="menu.html">Developer</a>
                            <a class="dropdown-item" href="menu_no-images.html">Application Support</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="reservation.html">Reservation</a>
                    </li>
                </ul>

                <!-- Navbar: Brand -->
                <a class="navbar-brand d-none d-lg-flex" href="index.html" style="font-family: Impact, fantasy; font:15px;">
                        Jagalab
                </a>

                <!-- Navbar navigation: Right -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="news-and-events.html">News &amp; Events</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="gallery.html">Gallery</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="contact-us.html">Contact</a>
                    </li>
                </ul>
            </div> <!-- / .navbar-collapse -->

        </div> <!-- / .container -->
        <div>
         <li>
                <a href="<?php echo base_url()?>index.php/login"><button>Login</button> </a>
        </li>
    </div>
    </nav>

   	<header class="bg-gradient" id="home">
        <div class="container mt-5">
            <h1>Special Region Of Yogyakarta</h1>
            <p class="tagline">Website Company Profil</p>
        </div>
        <div class="img-holder mt-3"><img src="https://i.pinimg.com/564x/7b/67/72/7b6772b14a0a692d0660b8d695bbe068.jpg" alt="" class="img-fluid" width="100px"></div>
    </header>
</section>
	<div class="section light-bg" id="features">
        <div class="container">
            <div class="section-title">
                <small>Jagalab</small>
                <h3>Special Region of Yogyakarta</h3>
            </div>
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-face-smile gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">Web programing</h4>
                                    <p class="card-text">Yogyakarta </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-settings gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">Application Suport</h4>
                                    <p class="card-text"> .. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-lg-4">
                    <div class="card features">
                        <div class="card-body">
                            <div class="media">
                                <span class="ti-lock gradient-fill ti-3x mr-3"></span>
                                <div class="media-body">
                                    <h4 class="card-title">Data Entry</h4>
                                    <p class="card-text">.. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
     <div class="section light-by">
    <?php foreach ($profil as $baris):?>
                <div class="btn-outline-*" style="margin-top: 2%;">
                <tr>
                    <td><?php echo $baris['no_profil'] ?></td>
                    <td><?php echo $baris['judul_profil'] ?></td>
                    <td><?php echo $baris['isi_profil']?></td>
                    <?php endforeach ?>
        </div>
        </tr>
</div>
       <div class="section light-by">
        <style>
            .light-by{
                background-color: #C7ECEE;
            }
        </style>
        <div class="container">
            <div class="tab">
              <button class="tablinks" onclick="openCity(event, 'London')">Employee Data</button>
              <button class="tablinks" onclick="openCity(event, 'Paris')">Jagalab</button>
              <button class="tablinks" onclick="openCity(event, 'Tokyo')">Jagalab</button>
            </div>

            <!-- Tab content -->
            <div id="London" class="tabcontent">
              <h3>Employee Data</h3>
              <p>Developer, Application Support, Data Entry</p>
            </div>

            <div id="Paris" class="tabcontent">
              <h3>Jagalab</h3>
              <p>All About </p> 
            </div>

            <div id="Tokyo" class="tabcontent">
              <h3>Jagalab</h3>
              <p>All About</p>
            </div>
        </div>
    </div>

<style>
    .tab {
    overflow: hidden;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
     position: center;
     margin: auto;
}

/* Style the buttons that are used to open the tab content */
.tab button {
    background-color: inherit;
    float: left;
    border: none;
    outline: none;
    cursor: pointer;
    padding: 14px 16px;
    transition: 0.3s;

}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    display: none;
    padding: 6px 12px;
    border: 1px solid #ccc;
    border-top: none;
    font-family: Verdana, sans-serif;
}
</style>
    <script>
        function openCity(evt, cityName) {
    // Declare all variables
    var i, tabcontent, tablinks;

    // Get all elements with class="tabcontent" and hide them
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }

    // Get all elements with class="tablinks" and remove the class "active"
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }

    // Show the current tab, and add an "active" class to the button that opened the tab
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
    </script>
 <div class="yuu-bp py-5" id="contact">
    <style>
        .yuu-bp{
            background-color: #95AFC0;
        }
    </style>
    <p class="contact">Contact us</h3>
    <div class="container sofp">
    <form action="index.php">
    <label for="fname">First Name</label>
    <input type="text" id="fname" name="firstname" placeholder="Your name..">

    <label for="lname">Last Name</label>
    <input type="text" id="lname" name="lastname" placeholder="Your last name..">

    <label for="country">Country</label>
    <select id="country" name="country">
      <option value="australia">Yogyakarta</option>
      <option value="canada">Semarang</option>
      <option value="usa">Sleman</option>
    </select>

    <label for="subject">Subject</label>
    <textarea id="subject" name="subject" placeholder="Write something.." style="height:50px"></textarea>

    <input type="submit" value="Submit">
  </form>
</div>
</div>
</div>

                <style>
                body {font-family: Arial, Helvetica, sans-serif;}

                input[type=text], select, textarea {
                    width: 100%;
                    padding: 12px;
                    border: 1px solid #ccc;
                    border-radius: 4px;
                    box-sizing: border-box;
                    margin-top: 6px;
                    margin-bottom: 16px;
                    resize: vertical;
                }

                input[type=submit] {
                    background-color: #4CAF50;
                    color: white;
                    padding: 12px 20px;
                    border: none;
                    border-radius: 4px;
                    cursor: pointer;
                }

                input[type=submit]:hover {
                    background-color: #45a049;
                }

                .sofp {
                    border-radius: 5px;
                    background-color: #f2f2f2;
                    padding: 5px;
                    margin-top: 10px;


                }

                .contact{
                        font-family: Impact, fantasy;
                        font-size: 30px;
                        text-align: center;
                        color: white;
                }
                </style>


  <div class="section light-bg">
        <div class="container">
            <h3>My Google Maps Demo</h3>
    <div id="map" style="width: 100%; height: 400px;"></div>
    <script>
      function initMap() {
        var uluru = {lat: -7.798627, lng: 110.413594};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3n5UyBG57u_6CyCMlUZ4zvMwltCme0iY&callback=initMap">
    </script>
</div>
</div>
  <div class="light-bi py-5" id="contact">
        <style>
            .light-bi{
                background-color: #95AFC0;
            }
        </style>
        <div class="container xio">
            <style>
                .xio {
                    border-radius: 5px;
                    background-color: #f2f2f2;
                    padding: 10px;
                }
            </style>
            <div class="row">
                <div class="col-lg-6 text-center text-lg-lee">
                    <style>
                        .text-lg-lee{
                            font-color:white;
                        }
                    </style>
                    <p class="mb-2"> <span class="ti-location-pin mr-2"></span> Banguntapan, Bantul, Yogyakarta</p>
                    <div class=" d-block d-sm-inline-block">
                        <p class="mb-2">
                            <span class="ti-email mr-2"></span> <a class="mr-4" href="Jagalab:Jagalab@gmail.com">jagalab@gmail.com</a>
                        </p>
                    </div>
                    <div class="d-block d-sm-inline-block">
                        <p class="mb-0">
                            <span class="ti-headphone-alt mr-2"></span> <a href="tel:51836362800">083-1236-9678</a>
                        </p>
                    </div>

                </div>
                <div class="col-lg-6">
                    <div class="social-icons">
                        <a href="#"><span class="ti-facebook"></span></a>
                        <a href="#"><span class="ti-twitter-alt"></span></a>
                        <a href="#"><span class="ti-instagram"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <section>
    <div class="light-bb py-5"> 
        <style>
            .light-bb{
                background-color: #C7ECEE;
            }
        </style>
    <div class="container omi">  
        <style>
            .omi{
                border-radius: 5px;
                background-color: #f2f2f2;
                padding:10px;
            }
        </style> 
    <footer class="my-5 text-center">
        <p class="mb-2"><small>COPYRIGHT © 2017. Jagalab Yogyakarta <a href="https://Jagalab.com">Jagalab</a></small></p>

        <small>
            <a href="#" class="m-2">PRESS</a>
            <a href="#" class="m-2">TERMS</a>
            <a href="#" class="m-2">PRIVACY</a>
        </small>
    </footer>
</div>
</div>
</section>
</body>
</html>
