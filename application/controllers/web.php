<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Web extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('m_model');
	}
 
	public function index(){		
		$data['judul'] = "Jagalab";
		$data['profil'] =  $this->m_model->getDataProfil();
		$this->load->view('index',$data);
		
	}
}


