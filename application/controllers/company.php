<?php
class Company extends CI_Controller {
	
	function __construct(){
		parent::__construct();		
		$this->load->model('m_model');
	}
 
	function index(){
		$data['judul_profil'] = 'Data Profil';
		$data['profil'] =  $this->m_model->getDataProfil();
		$this->load->view('v_profil', $data);
	}

	function tambah(){
		$nilai= array(
			'judul_profil' 	=> $this->input->post('title'), 
			'isi_profil'	=> $this->input->post('description'),
		);
		$oke = $this->m_model->insertDataProfil($nilai);
		if ($oke = 'true'){
			$data['judul_profil'] = 'Input Data Profil';
			$data['profil'] =  $this->m_model->getDataProfil();
			$this->load->view('v_profil', $data);
		}else{
			echo 'gagal simpan';
		}
		
	}

	function edit($no_profil = NULL) {
        $this->load->model('m_model');
        $data['profil'] = $this->m_model->DataProfil($no_profil);
        $this->load->view('edit', $data);    
    }

    function update(){
        $no_profil       = $this->input->post('no_profil');
        $judul_profil    = $this->input->post('judul_profil');
        $isi_profil      = $this->input->post('isi_profil');      
        
        $data=array(
            'no_profil'     => $no_profil,
            'judul_profil'  => $judul_profil,
            'isi_profil'    => $isi_profil,
                    
        );
        
        $this->m_model->updateprofil($data);   
    }


    function delete($no_profil){
        
        $this->m_model->deleteprofil($no_profil);    
    }
}
?>
