<?php
class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->CI =& get_instance();
	}

	public function index()
	{

		
			$this->load->helper(array('form', 'url'));
			$this->load->library('form_validation');
			$valid = $this->form_validation;
			 $user = $this->input->post('username');
			$pass  = $this->input->post('password');
			$valid->set_rules('username','Username', 'required');
			$valid->set_rules('password','Password','required');

			if ($valid->run()):
				$this->simple_login->login($user, $pass, base_url('admin'), base_url('login'));
			endif;

			$data = array('title' => 'Halaman Login');
			$this->load->view('v_login', $data);
		}
		

	public function logout()
	{
		$this->session->session_destroy();
		redirect('Web/index','refresh');
	}
}