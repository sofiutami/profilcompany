<?php 
 
class M_Model extends CI_Model{
	public function getDataProfil(){
		return $this->db->get('profil')->result_array();
	}
	public function insertDataProfil ($data){
		  $this->db->insert('profil', $data);
	}
	public function DataProfil ($no_profil){
		return $this->db->get_where('profil', array('no_profil'=>$no_profil))->row_array();
	}
	public function updateProfil($data = array()){
		$this->db->where('no_profil', $data['no_profil']);
	    $this->db->update('profil', $data);
	}
	public function deleteProfil($no_profil) {
		$this->db->where('no_profil', $no_profil)->delete('profil');
	}
}

?>